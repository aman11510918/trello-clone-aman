const API_KEY = 'YOUR KEY';
const TOKEN = 'YOUR TOKEN';
const LIST_ID = 'YOUR LIST ID';

/*------------------------for all cards of lists--------------------- */
const getCardId = async () => {
  const response = await fetch(`https://api.trello.com/1/lists/${LIST_ID}/cards?key=${API_KEY}&token=${TOKEN}`);
  const data = await response.json();
  return data.map(itemId => itemId["id"]);
}
const cardIdResult = getCardId();

/*---------------------------- for all checklist of cards --------------*/
const getChecklistIds = (item) => {
  return Promise.all(item.map(async current => {
    const response = await fetch(`https://api.trello.com/1/cards/${current}/checklists?key=${API_KEY}&token=${TOKEN}`);
    const data = await response.json();
    const data_1 = data.reduce((result, list) => {
      let newList = list.checkItems.map(l => {
        return {
          ...l,
          cardId: list.idCard
        };
      });
        result = result.concat(newList);
        return result;
      // return newList;
    }, []);
    return data_1;
  }))
}
let checkListId = cardIdResult.then(item => {
  return getChecklistIds(item);
});

/*------------------------------- for all Checklist-Item of Checklist ----------------*/
let checkListItem = checkListId.then(items => items)
checkListItem.then(current => {
  appendingList(current.flat())
  checkListDelete();
  checkListAdd();
  checkListCross();
});

function newCollection(newItems) {
  let mainDivision = document.createElement('div');
  mainDivision.className = 'todoList';
  mainDivision.setAttribute('data-cardId', newItems.cardId)
  mainDivision.setAttribute('data-itemId', newItems.id)
  mainDivision.setAttribute('data-checkListId', newItems.idChecklist)
  mainDivision.setAttribute('data-state', newItems.state)
  let divisionContent = `<input type="checkbox" class="check" >
                  <p class="lists" >${newItems.name} </p>
                  <i class="fas fa-window-close"></i>`
  mainDivision.innerHTML = divisionContent;
  return mainDivision
}

const appendingList = (current) => {
  let parentDivision = document.getElementById("tasks");

  current.forEach(value => {
    let newItem = newCollection(value);
    parentDivision.appendChild(newItem);
  })
}

/*---------------------------- Adding new items -----------------------*/
const addCallBack = () => {
  let textItem = document.getElementById('myInput');
  let name = textItem.value;
  let idChecklist = 'YOUR CHECKLIST ID'
  let cardId = 'YOUR CARD ID';
  fetch(`https://api.trello.com/1/checklists/${idChecklist}/checkItems?name=${name}&key=${API_KEY}&token=${TOKEN}`, {
      method: 'POST'
    })
    .then(response => response.json())
    .then(data => {
      let parent = document.getElementById('tasks')
      let element = newCollection({
        ...data,
        cardId: cardId
      });
      parent.prepend(element);
      textItem.value = "";
    });
}
const checkListAdd = () => {
  let dataAdd = document.getElementById('button');
  dataAdd.addEventListener('click', addCallBack)
}

/*------------------------delete checklist item------------------------*/
const deleteChecklistItems = (e) => {
  if (e.target.className === 'fas fa-window-close') {
    let element = e.target;
    let parentDivItemId = element.parentNode.attributes[2].value
    let parentDivChecklistId = element.parentNode.attributes[3].value
    fetch(`https://api.trello.com/1/checklists/${parentDivChecklistId}/checkItems/${parentDivItemId}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
      })
      .then(resp => {
        // console.log(resp.status)
        resp.json()
        if(resp.ok){
          element.parentElement.remove();
        }
      })
        .then(data => data)
  }
}
const checkListDelete = () => {
  let dataDelete = document.getElementById('tasks')
  dataDelete.addEventListener('click', deleteChecklistItems)
}

/*------------------------- CROSS the CHecklist Items------------------*/
const CrossChecklistItems = (e) => {

  if (e.target.className === "check") {
    let element = e.target;
    let parentDivCardId = element.parentNode.attributes[1].value
    let parentDivItemId = element.parentNode.attributes[2].value
    let parentDivState = element.parentNode.attributes[4].value
    let striking = element.nextSibling.nextElementSibling.style;

    if(element.checked === true){ 
        // console.log(element.checked)
      if(striking.textDecoration === '' || striking.textDecoration === 'none'){
        striking.textDecoration = 'line-through'
        striking.color = 'red'
      }
      if (parentDivState == 'incomplete') {
        parentDivState = 'complete'
        fetch(`https://api.trello.com/1/cards/${parentDivCardId}/checkItem/${parentDivItemId}?state=${parentDivState}&key=${API_KEY}&token=${TOKEN}`, {
          method: 'PUT'
        })
        // console.log(parentDivState)
      element.parentNode.setAttribute('data-state', parentDivState)
    }
  } else if (element.checked === false) {
        console.log(element.checked)
      if(striking.textDecoration === 'line-through'){
        striking.textDecoration = 'none'
        striking.color = 'black'
    }
      if (parentDivState == 'complete') {
        parentDivState = 'incomplete'
        fetch(`https://api.trello.com/1/cards/${parentDivCardId}/checkItem/${parentDivItemId}?state=${parentDivState}&key=${API_KEY}&token=${TOKEN}`, {
          method: 'PUT'
        })
      console.log(parentDivState)
      element.parentNode.setAttribute('data-state', parentDivState)
    }
  }
}
}
const checkListCross = () => {
  let checkCross = document.getElementById('tasks')
  checkCross.addEventListener('click', CrossChecklistItems)
}